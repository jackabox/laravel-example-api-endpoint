# CORS - Cross origin requests

```
composer require fruitcake/laravel-cors
php artisan vendor:publish --tag="cors"
```

Edit the config to allow `api/*` as path, and limit to the origin you have defined.

# Resource

Returns JSON representation of the data, can be mapped however you like.

```
php artisan make:resource State
```

# Endpoint

Return all the data to the endpoint

```
use App\Http\Resources\State as StateResource;
use App\State;

Route::get('/states', function () {
    return StateResource::collection(State::all());
});
```
