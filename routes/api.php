<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Return a resource of all the companies
Route::get('/companies', 'Api\CompanyController@index');
Route::get('/companies/{id}', 'Api\CompanyController@show');
Route::post('/companies/{id}', 'Api\CompanyController@update');

// Return a resource of all the contacts
Route::get('/contacts', 'Api\ContactController@index');
Route::get('/contacts/{id}', 'Api\ContactController@show');
Route::post('/contacts/{id}', 'Api\ContactController@update');
