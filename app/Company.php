<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use \Sushi\Sushi;

    protected $rows = [
        [
            'name' => 'Joe Bloggs',
            'email' => 'joe@bloggs.com',
            'active' => 1
        ],
        [
            'name' => 'Joanna Bloggs',
            'email' => 'joanna@bloggs.com',
            'active' => 1
        ],
        [
            'name' => 'Contact Name Example',
            'email' => 'example@company.com',
            'active' => 0
        ],
    ];
}
