<?php

namespace App\Http\Controllers\Api;

use App\Contact;
use App\Http\Controllers\Controller;
use App\Http\Resources\ContactResource;
use App\Http\Resources\ContactsResource;

class ContactController extends Controller
{
    public function index()
    {
        $name = request('name') ?: null;

        // Example paginated query
        $query = Contact::when($name, function ($q) use ($name) {
            return $q->where('name', 'LIKE', '%' . $name . '%');
        })
            ->paginate();

        // Returning the data
        return new ContactsResource($query);
    }

    public function show($id)
    {
        $contact = contact::findOrFail($id);
        return new ContactResource($contact);
    }

    public function update($id)
    {
        $contact = contact::findOrFail($id);

        // Example getting the request
        $name = request('name') ?: null;

        if ($name) {
            $contact->name = $name;
        }

        $contact->save();

        // Return it back after to update the endpoint.
        return new ContactResource($contact);
    }
}
