<?php

namespace App\Http\Controllers\Api;

use App\Company;
use App\Http\Resources\CompaniesResource;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyResource;

class CompanyController extends Controller
{
    public function index()
    {
        $name = request('name') ?: null;
        $active = request('active') ?: 1;

        // Example paginated query
        $query = Company::when($name, function ($q) use ($name) {
            return $q->where('name', 'LIKE', '%' . $name . '%');
        })
            ->where('active', $active)
            ->paginate();

        // Returning the data
        return new CompaniesResource($query);
    }

    public function show($id)
    {
        $company = Company::findOrFail($id);
        return new CompanyResource($company);
    }

    public function update($id)
    {
        $company = Company::findOrFail($id);

        // Example getting the request
        $name = request('name') ?: null;

        if ($name) {
            $company->name = $name;
        }

        $company->save();

        // Return it back after to update the endpoint.
        return new CompanyResource($company);
    }
}
