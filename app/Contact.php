<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use \Sushi\Sushi;

    protected $rows = [
        [
            'name' => 'Contact Joe',
            'email' => 'joe@contact.com'
        ],
        [
            'name' => 'Contact Fred',
            'email' => 'fred@contact.com'
        ]
    ];
}
